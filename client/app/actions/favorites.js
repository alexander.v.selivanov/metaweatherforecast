// @ts-check

import { createActions } from "redux-actions";

export const {
  loadFavorites,
  addFavorite,
  deleteFavorite,
  favoriteOperationError
} = createActions({
  LOAD_FAVORITES: favorites => favorites,
  ADD_FAVORITE: (woeid, title) => ({
    woeid,
    title
  }),
  DELETE_FAVORITE: woeid => ({
    woeid
  }),
  FAVORITE_OPERATION_ERROR: (err, message) => ({
    err,
    message
  })
});

export const fetchFavorites = () => dispatch => {
  fetch("/api/favorites/")
    .then(res => res.json())
    .then(favorites => {
      dispatch(loadFavorites(favorites));
    })
    .catch(error => {
      dispatch(favoriteOperationError(error));
    });
};

export const addFavoriteRequest = location => dispatch => {
  fetch(`/api/favorites/${location.woeid}/${location.title}`, {
    method: "PUT"
  })
    .then(res => res.json())
    .then(data => {
      dispatch(addFavorite(location.woeid, location.title));
    })
    .catch(error => {
      dispatch(favoriteOperationError(error));
    });
};

export const deleteFavoriteRequest = location => dispatch => {
  fetch(`/api/favorites/${location.woeid}`, {
    method: "DELETE"
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      dispatch(deleteFavorite(location.woeid));
    })
    .catch(error => {
      dispatch(favoriteOperationError(error));
    });
};
