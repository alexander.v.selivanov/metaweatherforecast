// @ts-check

import { createActions } from "redux-actions";

export const { clearForecast, loadForecast, loadForecastError } = createActions(
  {
    CLEAR_FORECAST: () => null,
    LOAD_FORECAST: forecast => forecast,
    LOAD_FORECAST_ERROR: (err, message) => ({
      err,
      message
    })
  }
);

export const fetchForecast = woeid => dispatch => {
  dispatch(clearForecast());
  fetch("/api/forecast/" + woeid)
    .then(res => res.json())
    .then(forecast => {
      dispatch(loadForecast(forecast));
    })
    .catch(error => {
      dispatch(loadForecastError(error));
    });
};
