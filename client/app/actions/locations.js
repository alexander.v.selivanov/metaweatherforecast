// @ts-check

import { createActions } from "redux-actions";

export const { searchLocationsSuccess, searchLocationsError } = createActions({
  SEARCH_LOCATIONS_SUCCESS: locations => locations,
  SEARCH_LOCATIONS_ERROR: (err, message) => ({
    err,
    message
  })
});

export const fetchLocations = searchQuery => dispatch => {
  fetch("/api/search/" + searchQuery)
    .then(res => res.json())
    .then(locations => {
      dispatch(searchLocationsSuccess(locations));
    })
    .catch(error => {
      dispatch(searchLocationsError(error));
    });
};
