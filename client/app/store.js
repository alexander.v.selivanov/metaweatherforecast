// @ts-check

import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import forecast from "./reducers/forecast";
import favorites from "./reducers/favorites";
import locations from "./reducers/locations";

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(applyMiddleware(thunkMiddleware));

const reducer = combineReducers({
  forecast,
  favorites,
  locations
});

export default createStore(reducer, enhancer);
