// @ts-check

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";

import { SEARCH_LOCATIONS, FAVORITES, FORECAST } from "./routerPaths";

import LocationListContainer from "./components/locations/LocationListContainer";
import ForecastDialogContainer from "./components/forecast/ForecastDialogContainer";
import FavoriteListContainer from "./components/favorites/FavoriteListContainer";

class App extends React.Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <Header />
          <Switch>
            <Route
              exact
              path={SEARCH_LOCATIONS}
              component={LocationListContainer}
            />
            <Route exact path={FAVORITES} component={FavoriteListContainer} />
            <Route path={FORECAST} component={ForecastDialogContainer} />
            <Route component={LocationListContainer} />
          </Switch>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
