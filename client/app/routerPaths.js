//@ts-check

export const SEARCH_LOCATIONS = "/search-locations";
export const FAVORITES = "/favorites";
export const FORECAST = "/forecast/:woeid";
