// @ts-check

import { handleActions } from "redux-actions";

import {
  searchLocationsSuccess,
  searchLocationsError
} from "../actions/locations";

const initialState = {
  locations: [],
  error: null
};

const reducer = handleActions(
  // @ts-ignore
  new Map([
    [
      searchLocationsSuccess,
      (state, action) => ({
        locations: action.payload,
        error: null
      })
    ],
    [
      searchLocationsError,
      (state, action) => ({
        locations: [],
        error: action.payload
      })
    ]
  ]),
  initialState
);

export default reducer;
