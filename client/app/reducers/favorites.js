// @ts-check

import { handleActions } from "redux-actions";

import {
  loadFavorites,
  addFavorite,
  deleteFavorite,
  favoriteOperationError
} from "../actions/favorites";

const initialState = {
  favorites: [],
  error: null
};

const reducer = handleActions(
  // @ts-ignore
  new Map([
    [
      loadFavorites,
      (state, action) => ({
        ...state,
        favorites: action.payload
      })
    ],
    [
      addFavorite,
      (state, action) => ({
        ...state,
        favorites: [...state.favorites, action.payload]
      })
    ],
    [
      deleteFavorite,
      (state, action) => ({
        ...state,
        favorites: state.favorites.filter(
          favorite => favorite.woeid != action.payload.woeid
        )
      })
    ],
    [
      favoriteOperationError,
      (state, action) => ({
        ...state,
        error: action.payload
      })
    ]
  ]),
  initialState
);

export default reducer;
