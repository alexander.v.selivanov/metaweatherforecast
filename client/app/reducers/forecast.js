// @ts-check

import { handleActions } from "redux-actions";

import {
  clearForecast,
  loadForecast,
  loadForecastError
} from "../actions/forecast";

const initialState = {
  forecast: null,
  error: null
};

const reducer = handleActions(
  // @ts-ignore
  new Map([
    [
      clearForecast,
      (state, action) => ({
        ...state,
        forecast: null
      })
    ],
    [
      loadForecast,
      (state, action) => ({
        ...state,
        forecast: action.payload
      })
    ],
    [
      loadForecastError,
      (state, action) => ({
        ...state,
        error: action.payload
      })
    ]
  ]),
  initialState
);

export default reducer;
