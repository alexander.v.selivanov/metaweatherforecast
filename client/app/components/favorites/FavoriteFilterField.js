// @ts-check

import React from "react";
import TextField from "@material-ui/core/TextField";

const FavoriteFilterField = ({ value, onFilterTextChange }) => (
  <TextField
    label="filter"
    value={value}
    onChange={onFilterTextChange}
    fullWidth
  />
);

export default FavoriteFilterField;
