// @ts-check

import React from "react";
import { connect } from "react-redux";

import { deleteFavoriteRequest } from "../../actions/favorites";
import FavoriteList from "./FavoriteList";

class FavoriteListContainer extends React.Component {
  handleDelete = location => {
    this.props.deleteFavoriteRequest(location);
  };

  render() {
    const { favorites, error } = this.props;
    return (
      <FavoriteList
        favorites={favorites}
        error={error}
        deleteFavorite={this.handleDelete}
      />
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    favorites: state.favorites.favorites,
    error: state.favorites.error
  };
};

const mapDispatcherToProps = {
  deleteFavoriteRequest
};

export default connect(
  mapStateToProps,
  mapDispatcherToProps
)(FavoriteListContainer);
