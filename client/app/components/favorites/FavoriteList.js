// @ts-check

import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import List from "@material-ui/core/List";
import { withStyles } from "@material-ui/core/styles";

import FavoriteFilterField from "./FavoriteFilterField";
import FavoriteListItem from "./FavoriteListItem";
import ErrorMessage from "../ErrorMessage";

import { FORECAST } from "../../routerPaths";

const styles = {
  content: {
    padding: 5,
    marginTop: 15
  }
};

class FavoriteList extends React.Component {
  state = {
    filterText: "",
    openForecastId: null
  };

  handleSearchTextChange = event => {
    const filter = event.target.value;
    this.setState({
      ...this.state,
      filterText: filter
    });
  };

  handleLocationClick = location => {
    this.setState({
      ...this.state,
      openForecastId: location.woeid
    });
  };

  render() {
    const openForecastId = this.state.openForecastId;
    if (openForecastId) {
      const url = FORECAST.replace(":woeid", openForecastId);
      return <Redirect push to={url} />;
    }

    const { classes, favorites, error } = this.props;
    const filterText = this.state.filterText;
    const filteredFavorites = filterText
      ? favorites.filter(favorite => favorite.includes(filterText))
      : favorites;
    return (
      <article className={classes.content}>
        <FavoriteFilterField
          value={filterText}
          onFilterTextChange={this.handleSearchTextChange}
        />

        {error ? (
          <ErrorMessage error={error} />
        ) : (
          <List>
            {filteredFavorites.map(favorite => (
              <FavoriteListItem
                key={favorite.woeid}
                title={favorite.title}
                onLocationClick={() => this.handleLocationClick(favorite)}
                onDeleteClick={() => this.props.deleteFavorite(favorite)}
              />
            ))}
          </List>
        )}
      </article>
    );
  }
}

FavoriteList.propTypes = {
  classes: PropTypes.object.isRequired,
  favorites: PropTypes.array.isRequired,
  error: PropTypes.object,
  deleteFavorite: PropTypes.func
};

export default withStyles(styles)(FavoriteList);
