// @ts-check

import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

const FavoriteListItem = ({ title, onLocationClick, onDeleteClick }) => (
  <ListItem role={undefined} dense button onClick={onLocationClick}>
    <ListItemText primary={title} />
    <ListItemSecondaryAction>
      <IconButton aria-label="Remove" onClick={onDeleteClick}>
        <DeleteIcon />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
);

export default FavoriteListItem;
