// @ts-check

import React from "react";
import { connect } from "react-redux";

import { fetchLocations } from "../../actions/locations";
import { addFavoriteRequest } from "../../actions/favorites";
import LocationList from "./LocationList";

class LocationListContainer extends React.Component {
  handleAddFavorite = location => {
    this.props.addFavoriteRequest(location);
  };

  render() {
    const { locations, favorites, error, fetchLocations } = this.props;
    return (
      <LocationList
        locations={locations}
        favorites={favorites}
        error={error}
        fetchLocations={fetchLocations}
        favoriteLocation={this.handleAddFavorite}
      />
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    locations: state.locations.locations,
    favorites: state.favorites.favorites,
    error: state.locations.error
  };
};

const mapDispatcherToProps = {
  fetchLocations,
  addFavoriteRequest
};

export default connect(
  mapStateToProps,
  mapDispatcherToProps
)(LocationListContainer);
