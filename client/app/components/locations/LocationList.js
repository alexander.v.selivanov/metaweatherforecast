// @ts-check

import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import List from "@material-ui/core/List";
import { withStyles } from "@material-ui/core/styles";

import LocationSearchField from "./LocationSearchField";
import LocationListItem from "./LocationListItem";
import ErrorMessage from "../ErrorMessage";

import { FORECAST } from "../../routerPaths";

const styles = {
  content: {
    padding: 5,
    marginTop: 15
  }
};

class LocationList extends React.Component {
  state = {
    locationSearchQuery: "",
    openForecastId: null
  };

  handleSearchTextChange = event => {
    const query = event.target.value;
    this.setState({ ...this.state, locationSearchQuery: query });
  };

  handleSearch = () => {
    const locationSearchQuery = this.state.locationSearchQuery;
    const fetchLocations = this.props.fetchLocations;
    if (fetchLocations && locationSearchQuery) {
      fetchLocations(locationSearchQuery);
    }
  };

  handleLocationClick = location => {
    this.setState({
      ...this.state,
      openForecastId: location.woeid
    });
  };

  render() {
    const openForecastId = this.state.openForecastId;
    if (openForecastId) {
      const url = FORECAST.replace(":woeid", openForecastId);
      return <Redirect push to={url} />;
    }

    const { classes, locations, favorites, error } = this.props;
    const query = this.state.locationSearchQuery;
    return (
      <article className={classes.content}>
        <LocationSearchField
          value={query}
          onSubmit={this.handleSearch}
          onSearchTextChange={this.handleSearchTextChange}
        />
        {error ? (
          <ErrorMessage error={error} />
        ) : (
          <List>
            {locations.map(location => (
              <LocationListItem
                key={location.woeid}
                title={location.title}
                showFavoriteButton={
                  !favorites.find(favorite => favorite.woeid == location.woeid)
                }
                onLocationClick={() => this.handleLocationClick(location)}
                onFavoriteClick={() => this.props.favoriteLocation(location)}
              />
            ))}
          </List>
        )}
      </article>
    );
  }
}

LocationList.propTypes = {
  classes: PropTypes.object.isRequired,
  locations: PropTypes.array,
  favorites: PropTypes.array,
  error: PropTypes.object,
  favoriteLocation: PropTypes.func
};

export default withStyles(styles)(LocationList);
