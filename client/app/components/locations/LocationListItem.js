// @ts-check

import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import StarIcon from "@material-ui/icons/Star";

const LocationListItem = ({
  title,
  showFavoriteButton,
  onLocationClick,
  onFavoriteClick
}) => (
  <ListItem role={undefined} dense button onClick={onLocationClick}>
    <ListItemText primary={title} />
    {showFavoriteButton && (
      <ListItemSecondaryAction>
        <IconButton aria-label="Favorite" onClick={onFavoriteClick}>
          <StarIcon />
        </IconButton>
      </ListItemSecondaryAction>
    )}
  </ListItem>
);

export default LocationListItem;
