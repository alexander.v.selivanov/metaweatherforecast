// @ts-check

import React from "react";
import TextField from "@material-ui/core/TextField";

const LocationSearchField = ({ value, onSearchTextChange, onSubmit }) => (
  <TextField
    label="search"
    helperText="Press Enter to start search"
    value={value}
    onChange={onSearchTextChange}
    onKeyPress={event => {
      var key = event.charCode || event.keyCode || 0;
      if (key == 13) {
        onSubmit();
      }
    }}
    fullWidth
  />
);

export default LocationSearchField;
