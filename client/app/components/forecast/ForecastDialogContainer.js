// @ts-check

import React from "react";
import { connect } from "react-redux";

import { fetchForecast } from "../../actions/forecast";
import ForecastDialogWithRouter from "./ForecastDialogWithRouter";

const ForecastDialogContainer = ({ forecast, error, fetchForecast }) => (
  <ForecastDialogWithRouter
    forecast={forecast}
    error={error}
    fetchForecast={fetchForecast}
  />
);

const mapStateToProps = (state, props) => {
  return {
    forecast: state.forecast.forecast,
    error: state.forecast.error
  };
};

const mapDispatcherToProps = {
  fetchForecast
};

export default connect(
  mapStateToProps,
  mapDispatcherToProps
)(ForecastDialogContainer);
