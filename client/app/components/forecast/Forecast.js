// @ts-check

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  forecastBlock: {
    display: "flex",
    alignItems: "stretch",
    flexWrap: "wrap",
    justifyContent: "center",
    alignContent: "stretch"
  },
  dayForecast: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "stretch",
    padding: 15,
    marginRight: 15,
    width: 150
  }
};

const locale = navigator.language || (navigator.languages || ["en"])[0];
const dateOptions = {
  weekday: "short",
  month: "short",
  day: "numeric"
};

const Forecast = ({ forecast, classes }) => {
  if (!forecast) {
    return null;
  }

  return (
    <div className={classes.forecastBlock}>
      {forecast.consolidated_weather.map(dayForecast => {
        const date = new Date(dayForecast.applicable_date).toLocaleDateString(
          locale,
          dateOptions
        );
        const weatherStateName = dayForecast.weather_state_name;
        const weatherState = dayForecast.weather_state_abbr;
        const imgUrl = `https://www.metaweather.com/static/img/weather/${weatherState}.svg`;
        const maxTemp = Math.round(Number(dayForecast.max_temp));
        const minTemp = Math.round(Number(dayForecast.min_temp));
        const windDirection = dayForecast.wind_direction_compass;
        const windSpeed = Math.round(Number(dayForecast.wind_speed));
        return (
          <div key={dayForecast.id} className={classes.dayForecast}>
            <p>
              <strong>{date}</strong>
            </p>
            <p>{weatherStateName}</p>
            <img width="32" height="32" src={imgUrl} />
            <p>
              Max: {maxTemp}
              &#8451;
            </p>
            <p>
              Min: {minTemp}
              &#8451;
            </p>
            <p>{windDirection}</p>
            <p>
              {windSpeed}
              mph
            </p>
          </div>
        );
      })}
    </div>
  );
};

Forecast.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Forecast);
