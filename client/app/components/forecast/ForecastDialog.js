// @ts-check

import React from "react";
import PropTypes from "prop-types";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { withStyles } from "@material-ui/core/styles";

import Forecast from "./Forecast";

const styles = {
  appBar: {
    position: "relative"
  },
  center: {
    margin: "0 auto"
  }
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const locale = navigator.language || (navigator.languages || ["en"])[0];
const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
};
const getDateString = forecastDateTimeString =>
  new Date(forecastDateTimeString).toLocaleDateString(locale, dateOptions);

const ForecastDialog = ({ classes, forecast, onClose }) => (
  <Dialog
    fullScreen
    open={true}
    onClose={onClose}
    TransitionComponent={Transition}
  >
    <AppBar className={classes.appBar}>
      <Toolbar>
        <IconButton color="inherit" onClick={onClose} aria-label="Close">
          <CloseIcon />
        </IconButton>
        <Typography variant="h6" color="inherit">
          {forecast ? (
            <span>Forecast for {forecast.title}</span>
          ) : (
            <span>Forecast loading...</span>
          )}
        </Typography>
        <Typography variant="h6" color="inherit" className={classes.center}>
          {forecast ? <span>{getDateString(forecast.time)}</span> : null}
        </Typography>
        <Button color="inherit" onClick={onClose}>
          close
        </Button>
      </Toolbar>
    </AppBar>

    {forecast && <Forecast forecast={forecast} />}
  </Dialog>
);

ForecastDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  forecast: PropTypes.object,
  onClose: PropTypes.func
};

export default withStyles(styles)(ForecastDialog);
