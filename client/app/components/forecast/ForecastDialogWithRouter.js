// @ts-check

import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import ForecastDialog from "./ForecastDialog";
import ErrorMessage from "../ErrorMessage";

class ForecastDialogWithRouter extends React.Component {
  componentDidMount() {
    const woeid = this.props.match.params.woeid;
    const fetchForecast = this.props.fetchForecast;
    fetchForecast(woeid);
  }

  handleClose = () => {
    this.props.history.goBack();
  };

  render() {
    const { forecast, error } = this.props;
    if (error) {
      return <ErrorMessage error={error} />;
    } else {
      return <ForecastDialog forecast={forecast} onClose={this.handleClose} />;
    }
  }
}

ForecastDialogWithRouter.propTypes = {
  match: PropTypes.object.isRequired,
  fetchForecast: PropTypes.func.isRequired,
  error: PropTypes.object
};

export default withRouter(ForecastDialogWithRouter);
