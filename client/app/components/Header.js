// @ts-check

import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import { SEARCH_LOCATIONS, FAVORITES } from "../routerPaths";

const styles = {
  logo: {
    marginLeft: -12,
    marginRight: 20
  },
  grow: {
    flexGrow: 1
  },
  link: {
    textDecoration: "none",
    color: "black"
  },
  activeLink: {
    textDecoration: "none",
    color: "black",
    backgroundColor: "silver"
  }
};

const Header = ({ classes }) => (
  <AppBar position="static" color="default">
    <Toolbar>
      <div className={classes.logo}>
        <img src="/favicon.png" alt="MetaWeather Forecast Example" />
      </div>
      <Typography variant="h6" color="inherit" className={classes.grow}>
        MetaWeather Forecast Example
      </Typography>
      <NavLink
        exact
        to={SEARCH_LOCATIONS}
        className={classes.link}
        activeClassName={classes.activeLink}
      >
        <Button color="inherit">Search Locations</Button>
      </NavLink>
      <NavLink
        exact
        to={FAVORITES}
        className={classes.link}
        activeClassName={classes.activeLink}
      >
        <Button color="inherit">Favorites</Button>
      </NavLink>
    </Toolbar>
  </AppBar>
);

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
