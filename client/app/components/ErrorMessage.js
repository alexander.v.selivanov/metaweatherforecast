// @ts-check

import React from "react";
import PropTypes from "prop-types";

const ErrorMessage = ({ error }) => (
  <div>
    <p>
      <strong>Error load locations</strong>
    </p>
    <p>{error.message}</p>
    <p>{error.err}</p>
  </div>
);

ErrorMessage.propTypes = {
  error: PropTypes.object.isRequired
};

export default ErrorMessage;
