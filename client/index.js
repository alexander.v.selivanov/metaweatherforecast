// @ts-check

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import App from "./app/App";
import store from "./app/store";

import { fetchFavorites } from "./app/actions/favorites";

store.dispatch(fetchFavorites());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
