// @ts-check

const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const outputDirectory = "build";

module.exports = {
  entry: path.join(__dirname, "client", "index.js"),
  output: {
    path: path.join(__dirname, outputDirectory),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: ["@babel/plugin-proposal-class-properties"]
          }
        }
      }
    ]
  },
  devServer: {
    port: 8080,
    open: true,
    historyApiFallback: true,
    proxy: {
      "/api/search": "http://localhost:3000",
      "/api/forecast": "http://localhost:3000",
      "/api/favorites": "http://localhost:3000"
    }
  },
  plugins: [
    new CleanWebpackPlugin([outputDirectory]),
    new HTMLWebpackPlugin({
      template: path.join(__dirname, "client", "index.html"),
      inject: false,
      favicon: path.join(__dirname, "client", "favicon.png")
    })
  ]
};
