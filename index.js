// @ts-check

const logger = require("./logger");
logger.info("Start application");

const path = require("path");
const Datastore = require("nedb");

const db = new Datastore({
  filename: path.join(__dirname, "db", "metaWeatherForecats.db")
});
logger.info("Load database");
db.loadDatabase(function(err) {
  if (err) {
    logger.info("Error load database. Error: " + err);
  }
});

const https = require("https");
const express = require("express");
const app = express();

const port = 3000;

app.get("/api/search/:searchText", (req, res) => {
  const searchText = req.params.searchText;
  logger.info(`Search for ${searchText}`);

  const url = `https://www.metaweather.com/api/location/search/?query=${searchText}`;
  https
    .get(url, httpsRes => {
      let locationList = "";
      httpsRes.on("data", chunk => {
        locationList += chunk;
      });
      httpsRes.on("end", function() {
        res.send(locationList);
      });
    })
    .on("error", err => {
      logger.error(
        `Error request location search for ${searchText}. Error: ${err}`
      );
      res.send({
        err,
        message: `Error request location search for ${searchText}.`
      });
    });
});

app.get("/api/forecast/:woeid", (req, res) => {
  const woeid = req.params.woeid;
  logger.info("Get forecast for id: " + woeid);

  const url = `https://www.metaweather.com/api/location/${woeid}/`;
  https
    .get(url, httpsRes => {
      let forecast = "";
      httpsRes.on("data", chunk => {
        forecast += chunk;
      });
      httpsRes.on("end", function() {
        res.send(forecast);
      });
    })
    .on("error", err => {
      logger.error(`Error request forecast for ${woeid}. Error: ${err}`);
      res.send({
        err,
        message: `Error request forecast for ${woeid}.`
      });
    });
});

app.get("/api/favorites", (req, res) => {
  logger.info("Favorites list");
  db.find({}, (err, favorites) => {
    if (err) {
      logger.error("Error find favorites. Error: " + err);
      res.send({ err, message: "Error find favorites" });
    } else {
      logger.info(`Favorites list find. Count: ${favorites.length}`);
      res.send(favorites);
    }
  });
});

const isFavotireExist = favorite =>
  new Promise((resolve, reject) => {
    db.find({ woeid: favorite.woeid }, (err, favorite) => {
      if (err) {
        reject(
          new Error(
            `Error find favorite (${favorite.woeid}, ${favorite.title})`
          )
        );
      } else {
        resolve(Boolean(favorite.length));
      }
    });
  });

app.put("/api/favorites/:woeid/:title", (req, res) => {
  logger.info("Add favorite");

  const favorite = {
    woeid: req.params.woeid,
    title: req.params.title
  };

  const errorSaveFavorite = err => {
    logger.error("Error save favorite. Error: " + err);
    res.send({ err, message: "Error save favorite" });
  };

  isFavotireExist(favorite)
    .then(exist => {
      if (exist) {
        const message = "Favorite already exist";
        logger.error(message);
        res.send({ err: null, message });
      } else {
        db.insert(favorite, (err, newFavorite) => {
          if (err) {
            errorSaveFavorite(err);
          } else {
            logger.info(
              `New favorite saved (${newFavorite.woeid}, ${newFavorite.title})`
            );
            res.send(newFavorite);
          }
        });
      }
    })
    .catch(err => {
      errorSaveFavorite(err);
    });
});

app.delete("/api/favorites/:woeid", (req, res) => {
  logger.info("Delete favorite");
  const woeid = req.params.woeid;
  db.remove({ woeid: woeid }, (err, numRemoved) => {
    if (err) {
      logger.error(`Error delete favorite (woeid: ${woeid}). Error: ${err}`);
      res.send({ err, message: "Error delete favorite. Woeid: " + woeid });
    } else {
      logger.info(`Favorite deleted (woeid: ${woeid})`);
      res.send({
        message: `Favorite deleted (woeid: ${woeid})`,
        num: numRemoved
      });
    }
  });
});

app.listen(port, () => {
  logger.info(`Server listening on port ${port}`);
});
